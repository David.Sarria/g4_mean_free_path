////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include "PrimaryGeneratorAction.hh"
#include "G4SystemOfUnits.hh"
#include "EarthMagField.hh"

using namespace std;

// interface avec code fortran igrf12 provenance du site : http://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
extern "C" {
    void
    igrf12syn_(int    *isv,
               double *date,
               int    *itype,
               double *alt,
               double *colat,
               double *elong,
               double *x,
               double *y,
               double *z,
               double *f,
               int    *ier);
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EarthMagField::EarthMagField() {}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EarthMagField::~EarthMagField() {}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void
EarthMagField::GetFieldValue(const double Point[3], double *Bfield) const
{
    int isv   = 0;
    int itype = 1;
    int ier = 0;
    double date = 2009;

    double alt, colat, elong;
    double Bx, By, Bz, f, lat, lon, Bvec[3];

    struct geodetic_coords geodetic;
    struct ecef_coords     ecef;

    ecef.x = Point[0] / km;
    ecef.y = Point[1] / km;
    ecef.z = Point[2] / km;

    // conversion ECEF to geodetic

    ecef_to_geodetic_olson(&ecef, &geodetic);

    lat = geodetic.lat * degree;   // radians
    lon = geodetic.lon * degree;   // radians

    G4double coslon, sinlon, sinlat, coslat;
    sincos(lon, &sinlon, &coslon); // important for performance
    sincos(lat, &sinlat, &coslat);

    //
    // // Get altitude from latitude
    // G4double altt = p*coslat + (z + e2*Nphi*sinlat)*sinlat - Nphi;

    alt = geodetic.alt; // itype=1, km
    // or altt=sqrt(x*x+y*y+z*z)
    //     itype = 1 if geodetic (spheroid)
    //     itype = 2 if geocentric (sphere)
    //     alt   = height in km above sea level if itype = 1
    //           = distance from centre of Earth in km if itype = 2 (>3485 km)

    elong = lon / degree;                                                           // degrees
    colat = (pi / 2. - lat) / degree;                                               // degrees

    // IGRF magnetic field
    igrf12syn_(&isv, &date, &itype, &alt, &colat, &elong, &Bx, &By, &Bz, &f, &ier); // gives NED (North East Down) components, in nT

    //     G4cout << Bx << " " << By << " " << Bz << G4endl;

    // NED (North East Down) to ECEF convertion

    // NOT USING EIGEN TO GET MORE PERFORMANCE

    // Local north direction      //Local east direction     // Local vertical (down)
    //        MAT_enu_ecef << -coslon*sinlat,            -sinlon,                  -coslon*coslat,
    //                        -sinlon*sinlat,             coslon,                  -sinlon*coslat,
    //                  coslat,                      0.   ,                     -sinlat;

    //        Bvec=MAT_enu_ecef*Bvec;

    Bvec[0] = -coslon * sinlat * Bx - sinlon * By - coslon * coslat * Bz;
    Bvec[1] = -sinlon * sinlat * Bx + coslon * By - sinlon * coslat * Bz;
    Bvec[2] = coslat * Bx - sinlat * Bz;

    const G4double nano_tesla_to_G4 = tesla * 1.e-9;
    Bfield[0] = Bvec[0] * nano_tesla_to_G4;
    Bfield[1] = Bvec[1] * nano_tesla_to_G4;
    Bfield[2] = Bvec[2] * nano_tesla_to_G4;

    /////// Simple dipole (very fast)
    //   const G4double B0 = 3.07e-5*tesla;
    //
    //   G4double x =Point[0] /m;
    //   G4double y =Point[1] /m;
    //   G4double z =Point[2] /m;
    //
    //   const G4double Re = earthRadius/m ; // in meters
    //
    //   G4double r = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
    //
    //   G4double fact = -B0 * pow(Re,3) / pow(r,5);
    //
    //   // Dipole magnetic field
    //
    //   // magnetic field
    //   Bfield[0]=fact*3.*x*z;
    //   Bfield[1]=fact*3.*y*z;
    //   Bfield[2]=fact*(2.*pow(z,2)-pow(x,2)-pow(y,2));

    // G4cout << Bvec[0] << Bvec[1] << Bvec[2]<< G4endl;
} // EarthMagField::GetFieldValue
