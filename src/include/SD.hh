#pragma once
#include "G4Track.hh"
#include "G4VSensitiveDetector.hh"
#include <fstream>
#include "Settings.hh"
#include "Analysis.hh" // singleton
#include <CLHEP/Units/SystemOfUnits.h>
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4UnitsTable.hh"

extern "C" {
#include <C/coordinates_conversions.h>
}

using namespace std;

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class SensitiveDet : public G4VSensitiveDetector
{

    public:
        SensitiveDet(G4String name, const G4int ID, const G4double ALT_RECORD_in_km);
        virtual ~SensitiveDet();

        virtual void Initialize(G4HCofThisEvent *HCE);
        virtual G4bool ProcessHits(G4Step *aStep, G4TouchableHistory *ROhist);
        virtual void EndOfEvent(G4HCofThisEvent *HCE);

        G4int getID_SD() const;

    private:

        int evtctr;
        G4StepPoint *thePrePoint = nullptr;

        TGFAnalysis *analysis = TGFAnalysis::getInstance();

        void given_altitude_particle_record(const G4Step *step);

        const G4int PDG_phot = 22;
        const G4int PDG_elec = 11;
        const G4int PDG_posi = -11;

        G4int ID_SD = 0;
        G4double ALT_RECORD_in_km = 0;

        std::vector < int > recorded_ID_inside_event_and_SD;
        
                            struct geocentric_coords geocentric_part_prepoint;
                    struct ecef_coords     ecef_part_prepoint;


        bool IDpart_not_recorded_yet(G4int ID);
        G4double Get_dist_rad(const G4double &lat, const G4double &lon, const G4double &alt_record);

};

