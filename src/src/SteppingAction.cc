////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include <RunAction.hh>
#include <SteppingAction.hh>
#include "globals.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "RegionInformation.hh"

XrayTelSteppingAction::XrayTelSteppingAction() {
}

XrayTelSteppingAction::~XrayTelSteppingAction() {}

void XrayTelSteppingAction::UserSteppingAction(const G4Step *aStep) {

    const G4int PDG = aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

    G4double kE = aStep->GetPreStepPoint()->GetKineticEnergy() / keV;

    G4double kE_after = aStep->GetPostStepPoint()->GetKineticEnergy() / keV;

    G4double loss = kE - kE_after;

    G4double step_length = aStep->GetStepLength() / meter;

    if ((PDG == PDG_photon) && (kE >= Settings::ENERGY)) { // killing particles with energy lower than threshold
//        G4cout << kE << " " << step_length << G4endl;
//        G4cout << kE << " " << kE_after << G4endl;
        sum_MFP += step_length;
        nb_total += 1.0;

        sum_loss += loss;

        if ((int(nb_total) % 50000) == 0) {
            G4cout << " Photon Mean free path estimate " << sum_MFP / nb_total << " (" << Settings::ENERGY << " keV; air at " << Settings::SOURCE_ALT
                   << " km altitude.)" << G4endl;
            G4cout << "average energy loss after one collision: " << sum_loss / nb_total << " keV" << G4endl;
        }
    }
}
