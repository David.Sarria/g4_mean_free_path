////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////
#include <Settings.hh>
#include <PrimaryGeneratorAction.hh>
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include <vector>
#include "G4ParticleDefinition.hh"
#include <iostream>

using namespace std;

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XrayTelPrimaryGeneratorAction::XrayTelPrimaryGeneratorAction(const G4String &particleName, G4double energy,
                                                             G4ThreeVector position, G4ThreeVector momentumDirection)
        : G4VUserPrimaryGeneratorAction(), fParticleGun(0) {
    G4int nofParticles = 1;

    fParticleGun = new G4ParticleGun(nofParticles);

    // default particle kinematic
    G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition *particle = particleTable->FindParticle(particleName);
    fParticleGun->SetParticleDefinition(particle);

    fParticleGun->SetParticleEnergy(energy);
    fParticleGun->SetParticlePosition(position);
    fParticleGun->SetParticleMomentumDirection(momentumDirection);
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XrayTelPrimaryGeneratorAction::~XrayTelPrimaryGeneratorAction() {
    delete fParticleGun;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XrayTelPrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent) {

    // this function is called at the begining of event
    // 1/E
    //     G4double energy=MinEner*(pow(((MaxEner)/(MinEner)),G4UniformRand()))

    ////////////// ENERGY /////////////////

    G4double energy = Settings::ENERGY * keV;

    ////////////// assignments /////////////////

    fParticleGun->SetParticleTime(0.0);

    fParticleGun->SetParticlePosition(G4ThreeVector{0, 0, 0});
    fParticleGun->SetParticleMomentumDirection(G4ThreeVector{1.0, 0, 0});
    fParticleGun->SetParticleEnergy(energy);

    G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition *particle = particleTable->FindParticle("gamma");
    fParticleGun->SetParticleDefinition(particle);

    fParticleGun->GeneratePrimaryVertex(anEvent);
} // XrayTelPrimaryGeneratorAction::GeneratePrimaries

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

double XrayTelPrimaryGeneratorAction::BrokenPL(double p1, double p2, double ec, double x) {
    double broken_PL = 0;

    if (x < ec) {
        broken_PL = std::pow(x, p1);
    } else if (x >= ec) {
        broken_PL = std::pow(ec, p1 - p2) * std::pow(x, p2);
    }

    return broken_PL;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double XrayTelPrimaryGeneratorAction::Sample_one_RREA_gammaray_energy(G4double MinEner, G4double MaxEner, G4double cut_ener) {
    // random samples the energy of one RREA gamma ray
    // RREEA gamma spectrum is approximately = 1/E * exp (-E / 7.3MeV)

    //     G4double energy=MinEner*(pow(((MaxEner)/(MinEner)),G4UniformRand())); // code for sampling 1/E

    // (rejection method)

    G4double H = cut_ener;

    G4double pMax = 1. / MinEner * exp(-MinEner / H);
    G4double pMin = 1. / MaxEner * exp(-MaxEner / H);

    G4double pOfeRand = 0.0;
    G4double pRand = 1.0;

    G4double eRand;

    while (pOfeRand < pRand) // rejection
    {
        pRand = pMin + (pMax - pMin) * G4UniformRand();
        eRand = MinEner + (MaxEner - MinEner) * G4UniformRand();

        pOfeRand = 1.0 / eRand * exp(-eRand / H);
    }

    return eRand;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
