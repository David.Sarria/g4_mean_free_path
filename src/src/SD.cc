#include "SD.hh"
#include "G4HCofThisEvent.hh"
#include "G4TouchableHistory.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4StepPoint.hh"
#include "G4EventManager.hh"
#include "G4Event.hh"
#include "CLHEP/Units/SystemOfUnits.h"

// ONE DIFFERENT SENSITIVE DETECTOR BY RECORD ALTITUDE

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SensitiveDet::SensitiveDet(G4String name, const G4int ID, const G4double alt_in) : G4VSensitiveDetector(name)
{
    this->ID_SD = ID;
    this->ALT_RECORD_in_km = alt_in;

    // initialization
    recorded_ID_inside_event_and_SD.clear();
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SensitiveDet::~SensitiveDet()
{
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SensitiveDet::Initialize(G4HCofThisEvent *) // executed at begin of each event
{
    // if it is a new event, clear the recorded ID vector

    recorded_ID_inside_event_and_SD.clear();
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool SensitiveDet::ProcessHits(G4Step *aStep, G4TouchableHistory * /*ROhist*/)
{

    given_altitude_particle_record(aStep);

    return true;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SensitiveDet::EndOfEvent(G4HCofThisEvent * /*HCE*/)
{
    // RK : see EndOfEventAction method in EventAction.cc
    recorded_ID_inside_event_and_SD.clear(); // redundant, but jsut to be safe
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SensitiveDet::given_altitude_particle_record(const G4Step *step)
{
    G4Track *track = step->GetTrack();
    const G4int type_number = track->GetParticleDefinition()->GetPDGEncoding();

    if (Settings::RECORD_ELEC_POSI_ONLY)
        {
            if (type_number == 22) return; // WARNING: no photon is recorded
        }

    //    const G4int PDG_nb = aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

    //    if (PDG_nb == PDG_phot) return; // WARNING: keeping only electrons and positrons

    thePrePoint = step->GetPreStepPoint();

    if (thePrePoint->GetStepStatus() == fGeomBoundary) // if the particle has just entered the volume
        {
            const G4double geocentric_alt_record = ALT_RECORD_in_km * km;

            const G4int ID = track->GetTrackID();

           // if (IDpart_not_recorded_yet(ID))
           //     {
                    // getting geodetic coordinates

                    ecef_part_prepoint.x = thePrePoint->GetPosition().x() / km;
                    ecef_part_prepoint.y = thePrePoint->GetPosition().y() / km;
                    ecef_part_prepoint.z = thePrePoint->GetPosition().z() / km;

                    ecef_to_geocentric(&ecef_part_prepoint, &geocentric_part_prepoint);

                    const G4double dist_rad = Get_dist_rad(geocentric_part_prepoint.lat, geocentric_part_prepoint.lon, geocentric_alt_record);

                    // PDG encoding is 22 for gamma, 11 for electrons, -11 for positrons

                    const G4double energy = thePrePoint->GetKineticEnergy() / keV;
                    const G4double time = thePrePoint->GetGlobalTime() / microsecond;

                    //    if ((type_number != gamma_number) || (energy <= 0.))
                    //        {
                    //            return;
                    //        }

                    G4String creator_process = "initial";

                    if (track->GetCreatorProcess())
                        {
                           creator_process = track->GetCreatorProcess()->GetProcessName();
                        }

                    //                    if (creator_process == "annihil" && energy > 511.0) return;
                   // recorded_ID_inside_event_and_SD.push_back(ID);

                    analysis->save_in_output_buffer(type_number, time, energy, geocentric_alt_record / km, dist_rad / km, ID, creator_process);
                    
                    step->GetTrack()->SetTrackStatus(fStopAndKill);
              //  }
        }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int SensitiveDet::getID_SD() const
{
    return ID_SD;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

bool SensitiveDet::IDpart_not_recorded_yet(G4int ID)
{
    if (recorded_ID_inside_event_and_SD.empty()) return true;

    if (std::find(recorded_ID_inside_event_and_SD.begin(), recorded_ID_inside_event_and_SD.end(), ID) != recorded_ID_inside_event_and_SD.end())
        {
            return false; /* v contains x */
        }
    else
        {
            return true;  /* v does not contain x */
        }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double SensitiveDet::Get_dist_rad(const G4double &lat, const G4double &lon, const G4double &alt_record)
{
    // getting the radial distance (along curve parrallel to Earth surface)

    // Equirectangular approximation -> leads to "effective speed" of the output data that can be slightly greater than the speed of light

    //    G4double RR = (settings->EarthRadius() + alt_record) ;
    //    G4double delta_lambda = (settings->SourceLong() - lon) * degree;
    //    G4double sum_phi = (lat + settings->SourceLat()) * degree;
    //
    //    G4double xxx = delta_lambda * cos((sum_phi) / 2.);
    //    G4double yyy = (settings->SourceLat() - lat) * degree;
    //    G4double dist_rad = sqrt(pow(xxx, 2) + pow(yyy, 2)) * RR;

    // haversine formula

    const G4double RR           = (Settings::earthRadius + alt_record);

    const G4double phi1         = (lat) * degree;
    const G4double phi2         = (Settings::SOURCE_LAT) * degree;
    const G4double delta_phi    = (phi2 - phi1);

    const G4double delta_lambda = (Settings::SOURCE_LONG - lon) * degree;

    const G4double aa = sin(delta_phi / 2.) * sin(delta_phi / 2.) +
                        cos(phi1) * cos(phi2) * sin(delta_lambda / 2.) * sin(delta_lambda / 2.);

    const G4double cc = 2. * atan2(sqrt(aa), sqrt(1. - aa));

    // G4double cc = 2. * std::asin(std::min(1., sqrt(aa)));

    const G4double dist_rad = RR * cc;

    return dist_rad;
}

// // linear interpolation to get the coordinates at the output altitude (alt_tmp)
//record_coords record_cords = get_interpolated_point_at_detAlt(position_pre, position_post, alt_pre, alt_post,
//                             alt_detect_tmp, time_pre);
//    const RegionInformation *thePreRInfo = (RegionInformation *)(thePreLV->GetRegion()->GetUserInformation());
//    // skip the track + return (skipping the rest of the code in this method) if volume is not in the considered atmosphere
//    if (thePreRInfo->is_World())
//        {
//            track->SetTrackStatus(fStopAndKill);
//            return;
//        }
