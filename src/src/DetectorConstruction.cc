////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include <DetectorConstruction.hh>
#include <fortran.hh>

using namespace std;

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// C     INPUT VARIABLES:
// C        IYD - YEAR AND DAY AS YYDDD (day of year from 1 to 365 (or 366))
// C              (Year ignored in current model)
// C        SEC - UT(SEC)
// C        ALT - ALTITUDE(KM)
// C        GLAT - GEODETIC LATITUDE(DEG)
// C        GLONG - GEODETIC LONGITUDE(DEG)
// C        STL - LOCAL APPARENT SOLAR TIME(HRS; see Note below)
// C        F107A - 81 day AVERAGE OF F10.7 FLUX (centered on day DDD)
// C        F107 - DAILY F10.7 FLUX FOR PREVIOUS DAY
// C        AP - MAGNETIC INDEX(DAILY) OR WHEN SW(9)=-1. :
// C           - ARRAY CONTAINING:
// C             (1) DAILY AP
// C             (2) 3 HR AP INDEX FOR CURRENT TIME
// C             (3) 3 HR AP INDEX FOR 3 HRS BEFORE CURRENT TIME
// C             (4) 3 HR AP INDEX FOR 6 HRS BEFORE CURRENT TIME
// C             (5) 3 HR AP INDEX FOR 9 HRS BEFORE CURRENT TIME
// C             (6) AVERAGE OF EIGHT 3 HR AP INDICIES FROM 12 TO 33 HRS PRIOR
// C                    TO CURRENT TIME
// C             (7) AVERAGE OF EIGHT 3 HR AP INDICIES FROM 36 TO 57 HRS PRIOR
// C                    TO CURRENT TIME
// C        MASS - MASS NUMBER (ONLY DENSITY FOR SELECTED GAS IS
// C                 CALCULATED.  MASS 0 IS TEMPERATURE.  MASS 48 FOR ALL.
// C                 MASS 17 IS Anomalous O ONLY.)
// C
// C     NOTES ON INPUT VARIABLES:
// C        UT, Local Time, and Longitude are used independently in the
// C        model and are not of equal importance for every situation.
// C        For the most physically realistic calculation these three
// C        variables should be consistent (STL=SEC/3600+GLONG/15).
// C        The Equation of Time departures from the above formula
// C        for apparent local time can be included if available but
// C        are of minor importance.
// c
// C        F107 and F107A values used to generate the model correspond
// C        to the 10.7 cm radio flux at the actual distance of the Earth
// C        from the Sun rather than the radio flux at 1 AU. The following
// C        site provides both classes of values:
// C        ftp://ftp.ngdc.noaa.gov/STP/SOLAR_DATA/SOLAR_RADIO/FLUX/
// C
// C        F107, F107A, and AP effects are neither large nor well
// C        established below 80 km and these parameters should be set to
// C        150., 150., and 4. respectively.
// C
// C     OUTPUT VARIABLES:
// C        D(1) - HE NUMBER DENSITY(CM-3)
// C        D(2) - O NUMBER DENSITY(CM-3)
// C        D(3) - N2 NUMBER DENSITY(CM-3)
// C        D(4) - O2 NUMBER DENSITY(CM-3)
// C        D(5) - AR NUMBER DENSITY(CM-3)
// C        D(6) - TOTAL MASS DENSITY(GM/CM3)
// C        D(7) - H NUMBER DENSITY(CM-3)
// C        D(8) - N NUMBER DENSITY(CM-3)
// C        D(9) - Anomalous oxygen NUMBER DENSITY(CM-3)
// C        T(1) - EXOSPHERIC TEMPERATURE
// C        T(2) - TEMPERATURE AT ALT

// IYD,SEC,ALT,GLAT,GLONG,STL,F107A,F107,AP,MASS,D,T

// extrernal fortran subroutine to get MSIS atmospheric densities
extern "C"
{
void gtd7_(INTEGER &IYD,  // YEAR AND DAY AS YYDDD (day of year from 1 to 365 (or 366))
           REAL &SEC,     // UT(SEC)
           REAL &ALT,     // ALTITUDE(KM)
           REAL &GLAT,    // GEODETIC LATITUDE(DEG)
           REAL &GLONG,   // GEODETIC LONGITUDE(DEG)
           REAL &STL,     // LOCAL APPARENT SOLAR TIME
           REAL &F107A,   // 81 day AVERAGE OF F10.7 FLUX (centered on day DDD
           REAL &F107,    // DAILY F10.7 FLUX FOR PREVIOUS DAY
           REAL &AP,      // MAGNETIC INDEX(DAILY)
           INTEGER &MASS, // MASS NUMBER
           REAL *D,
           REAL *T); // OUTPUT VARIABLES temperatures
}

TGFDetectorConstruction::TGFDetectorConstruction() {
    logicalWorld = nullptr;
    physicalWorld = nullptr;

}

TGFDetectorConstruction::~TGFDetectorConstruction() = default;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

G4VPhysicalVolume *TGFDetectorConstruction::Construct() {
    // cleaning geometry
    G4GeometryManager::GetInstance()->OpenGeometry();
    G4PhysicalVolumeStore::Clean();
    G4LogicalVolumeStore::Clean();
    G4SolidStore::Clean();

    G4Material *my_air = Construct_Atmos_layer_Material(Settings::SOURCE_ALT * km);


    solidWorld = new G4Orb("world_S", 100.0 * km);
    logicalWorld = new G4LogicalVolume(solidWorld,
                                       my_air,
                                       "world_L");

    // Physical volume
    physicalWorld = new G4PVPlacement(nullptr, G4ThreeVector(), "world_P", // name (2nd constructor)
                                      logicalWorld,                        // logical volume
                                      nullptr,                             // mother volume
                                      false,                               // no boolean operation
                                      0);                                  // copy number

    G4VisAttributes *VisAttWorld = new G4VisAttributes(G4Colour(204 / 255., 255 / 255., 255 / 255.));
    logicalWorld->SetVisAttributes(VisAttWorld);
    // setting default (world) region info
    G4Region *defaultRegion = (*(G4RegionStore::GetInstance()))[0]; // the default (world) region is index 0 in the region store
    auto *defaultRInfo = new RegionInformation();
    defaultRInfo->Set_World();
    defaultRegion->SetUserInformation(defaultRInfo);

    G4double innerRad = 0;
    G4double outerRad = 0;
    G4int id_SD = 0;

    G4cout << "Geometry built successfully." << G4endl;
    return physicalWorld;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// Caltulating materials of the atmopsheric layers, based on the MSIS C++ model integrated to this code
// ref : https://ccmc.gsfc.nasa.gov/modelweb/atmos/nrlmsise00.html

G4Material *
TGFDetectorConstruction::Construct_Atmos_layer_Material(G4double altitude) {
    G4Material *Air = 0;
    // Vaccum
    G4NistManager *man = G4NistManager::Instance();
    G4Material *vaccum = man->FindOrBuildMaterial("G4_Galactic");

    const double altitude_in_km = altitude / km; // geocentric altitude

    INTEGER input_iyd = 18096; // IYD - YEAR AND DAY AS YYDDD
    REAL input_sec = 29000.0;
    REAL input_alt = (REAL) altitude_in_km;
    REAL input_g_lat = (REAL) Settings::SOURCE_LAT;
    REAL input_g_long = (REAL) Settings::SOURCE_LONG;
    REAL input_lst = 16.0;
    REAL input_f107A = 150.0;
    REAL input_f107 = 150.0;
    REAL input_ap = 4.0;
    INTEGER input_mass = 48;
    REAL output_D[9];
    REAL output_T[2];
    // G4cout << altitude << G4endl;
    gtd7_(input_iyd, input_sec, input_alt, input_g_lat, input_g_long, input_lst, input_f107A, input_f107,
          input_ap, input_mass, output_D, output_T); //

    // MSIS,
    if (std::isnan(output_D[5]) || std::isinf(isnan(output_D[5]))) {
        G4cout << "ERROR : density from gtd7_ is NaN. Aborting" << G4endl;
        std::abort();
    }

    G4cout << "Air density:" << output_D[5] << " g/cm3 for " << altitude_in_km << " km altitude" << G4endl;

    G4double density_air =
            output_D[5] * g / cm3; // getting density and converting it to the GEANT4 system of unit

    std::this_thread::sleep_for(std::chrono::seconds(3));

    Air = man->BuildMaterialWithNewDensity("myAir", "G4_AIR", density_air);

    return Air;
}
