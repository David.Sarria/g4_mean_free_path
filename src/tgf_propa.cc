////////////////////////////////////////////////////////////////////////////////

// /* GEANT4 code for propagation of gamma-rays, electron and positrons in Earth's atmosphere */
//
// //
// // ********************************************************************
// // * License and Disclaimer                                           *
// // *                                                                  *
// // * The  Geant4 software  is  copyright of the Copyright Holders  of *
// // * the Geant4 Collaboration.  It is provided  under  the terms  and *
// // * conditions of the Geant4 Software License,  included in the file *
// // * LICENSE and available at  http://cern.ch/geant4/license .  These *
// // * include a list of copyright holders.                             *
// // *                                                                  *
// // * Neither the authors of this software system, nor their employing *
// // * institutes,nor the agencies providing financial support for this *
// // * work  make  any representation or  warranty, express or implied, *
// // * regarding  this  software system or assume any liability for its *
// // * use.  Please see the license in the file  LICENSE  and URL above *
// // * for the full disclaimer and the limitation of liability.         *
// // *                                                                  *
// // * This  code  implementation is the result of  the  scientific and *
// // * technical work of the GEANT4 collaboration.                      *
// // * By using,  copying,  modifying or  distributing the software (or *
// // * any work based  on the software)  you  agree  to acknowledge its *
// // * use  in  resulting  scientific  publications,  and indicate your *
// // * acceptance of all terms of the Geant4 Software license.          *
// // ********************************************************************
////////////////////////////////////////////////////////////////////////////////

#include <DetectorConstruction.hh>
#include <PhysicsList.hh>
#include "Settings.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4String.hh"
#include "Randomize.hh"
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif // ifdef G4VIS_USE

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif // ifdef G4UI_USE

#include <RunAction.hh>

#include <EventAction.hh>
#include <SteppingAction.hh>

#include <cstdlib>
#include <iostream>
#include <vector>

#include "G4PhysListFactory.hh"

#include <PrimaryGeneratorAction.hh>
#include "Bline_tracer/G4BlineTracer.hh"

#include <chrono>
using namespace std;

/////////////////////////////////////////////////////////

double get_wall_time()
{
    std::chrono::high_resolution_clock m_clock;
    double time = std::chrono::duration_cast<std::chrono::seconds>(
                      m_clock.now().time_since_epoch())
                      .count();
    return time;
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    double wall0 = get_wall_time();

    Settings::record_altitudes.push_back(400.);

    //   settings->add_OutputAltitude(14.);
    //   settings->add_OutputAltitude(16.);
    //   settings->add_OutputAltitude(18.);
    //   settings->add_OutputAltitude(20.);

    G4String Mode = "run";

    G4String number_st;

    // input parameters
    //     std::cout << "Argument list" << std::endl;
    //     for (int i = 0; i < argc; ++i) {
    //         std::cout << argv[i] << std::endl;
    //     }

    std::chrono::high_resolution_clock m_clock;
    long start = std::chrono::duration_cast<std::chrono::nanoseconds>(
                     m_clock.now().time_since_epoch())
                     .count();

    Settings::Rand_seed = start;

    if (argc > 3)
    {
        Mode = "run";
        number_st = argv[1];
    }
    else
    {
        Mode = "run";
        number_st = "10000000";
    }

    // choose the Random engine and give seed
    G4Random::setTheEngine(new CLHEP::MTwistEngine);
    G4Random::setTheSeed(Settings::Rand_seed);

    // Construct the default run manager
    G4RunManager *runManager = new G4RunManager;

    // Construct the helper class to manage the electric field &
    // the parameters for the propagation of particles in it.

    // set mandatory initialization classes
    runManager->SetUserInitialization(new TGFDetectorConstruction);

    runManager->SetUserInitialization(new XrayTelPhysicsList); // for using defined physics list

    // for using Reference physics list

    //   G4PhysListFactory*physListFactory= new G4PhysListFactory();
    // G4VUserPhysicsList *physicsList=physListFactory->GetReferencePhysList("LBE_LIV"); // low and high energy physics with livermore
    //                                                               // _PEN for PENELOPE

    //  runManager->SetUserInitialization(physicsList);

    // std::vector<G4String> v =physListFactory->AvailablePhysLists();

    // set mandatory user action class
    runManager->SetUserAction(new XrayTelPrimaryGeneratorAction);
    runManager->SetUserAction(new XrayTelRunAction);
    runManager->SetUserAction(new XrayTelEventAction);
    runManager->SetUserAction(new XrayTelSteppingAction);

    //    G4BlineTracer* theBlineTool = new G4BlineTracer();

    // Initialize G4 kernel
    runManager->Initialize();
    G4cout << G4endl << "Initialization OK" << G4endl;

    // get the pointer to the User Interface manager
    G4UImanager *UImanager = G4UImanager::GetUIpointer();

    if (Mode == "visu")
    {
#ifdef G4VIS_USE
        G4VisManager *visManager = new G4VisExecutive;
        visManager->Initialize();
#endif // ifdef G4VIS_USE

#ifdef G4UI_USE
        G4UIExecutive *ui = new G4UIExecutive(argc, argv);
#ifdef G4VIS_USE
        UImanager->ApplyCommand("/control/execute vis.mac");
#endif // ifdef G4VIS_USE
        ui->SessionStart();
        delete ui;
#endif // ifdef G4UI_USE
#ifdef G4VIS_USE
        delete visManager;
#endif // ifdef G4VIS_USE
    }
    else if (Mode == "run")
    {
        UImanager->ApplyCommand("/run/beamOn " + number_st);
    }

    delete runManager;

    double wall1 = get_wall_time();

    G4cout << G4endl << "WALL TIME TAKEN : " << wall1 - wall0 << " seconds " << G4endl;

    return 0;
} // main
